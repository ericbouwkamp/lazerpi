#include <cstring>
#include <iostream>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h> // sockaddr_in Structure
#include <fcntl.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <iomanip>
using namespace std;

int main()
{
	struct sockaddr_in server;
	int socketHandle = socket(AF_INET, SOCK_DGRAM, 0);
	unsigned int ack[2] = {htonl(0xDEADBEEF), htonl(0xDEADBEEF)};
	unsigned int vals[2];
	int received = 0;
	sockaddr_in client;
	socklen_t addrlen = sizeof(client);	
	
	server.sin_family = AF_INET;
	server.sin_port = htons(5000);
	inet_pton(AF_INET, "192.168.1.200", &server.sin_addr);
	
	bind(socketHandle, (struct sockaddr*)&server, sizeof(server));

	sendto(socketHandle, &ack, 8, 0, (struct sockaddr *)&server, addrlen);
	
	received = recvfrom(socketHandle, &vals, 8, 0, (struct sockaddr *)&client, &addrlen);
	cout << received << endl;
	cout << vals[0] << endl;
	cout << vals[1] << endl;
}