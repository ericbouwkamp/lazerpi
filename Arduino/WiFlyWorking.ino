#include <SPI.h>
#include <WiFly.h>
#include <arpa/inet.h>

boolean commandMode = false;   //is the Wifly presently in command mode?


byte byteArray[8];

void setup() {
  unsigned long command = 0;
  unsigned long data = 0;
  Serial.begin(9600);
  SpiSerial.begin();
  
  
  createCommand(&command, &data, 12, 34, 31 , 31, 910);
  splitCommand(byteArray, command, data);
  configureWifly();
  Serial.println("Now Sending Data to Central Hub");
}


void loop() {
    //it's ok to send now.
    delay(250);
    SpiSerial.write(byteArray,8);

//    SpiSerial.write(0xd);
    //set wait period!
}

void configureWifly()
{
    delay(333);
    Serial.print("Entering Command Mode: ");
    //SET UP WIFLY
    runCommand("Enter Command Mode",false); //enter command mode (prints $$$).
    Serial.print(" DONE\n");
    
    //set dhcp mode
    runCommand("set ip dchp 1",true); //set DHCP mode   /// THESE ARE HOW YOU PROGRAM THE WIFLY MODULE, YOU CAN DO THIS THROUGH TELNET OR COMMAND LINE BUT THIS WAY IS MUCH MUCH FASTER
    
    //set the SSID
    runCommand("set wlan ssid LazerOps",true); //set ssid

    // set Wlan Passphrase
    runCommand("set wlan phrase 0",true); //set password

    // Set IP Host Address
    runCommand("set ip host 192.168.1.200",true); //set the host IP address.

    // Set IP LocalPort
    runCommand("set ip localport 2000",true); //set the listen port

    // Set IP RemotePort
    runCommand("set ip remote 5000",true); //set the send port
  
    // Set IP Protocol to UDP (0)
    runCommand("set ip protocol 1",true); //set the TCP and UDP

    // Set System Sleep timer to Disabled
    runCommand("set sys sleep 0",true); 
    
    // Set System Comm Timer to Longest Possible
    runCommand("set comm timer 429496795",true); 

    // Set System Wake Timer to 0
    runCommand("set sys wake 0",true); 

    // Set Comm Match to 0    
    runCommand("set comm match 0",true); 
    
    // Set Comm Time to 0
    runCommand("set comm time 0",true);
    
    // Set Comm Size to 8
    runCommand("set comm size 8",true);
    
    // Set broadcast interval to 0
    runCommand("set broadcast interval 0",true);
    
    // Send Command to Auto Join
    runCommand("set wlan join 1",true);
    
    // Set 
    // Send Command to Save Settings to Module
    Serial.print("Saving Configuration: ");
    runCommand("save",false);
    Serial.print(" DONE\n");
    
    Serial.print("Now Rebooting ");
    runCommand("reboot",false);
    for (int i = 10; i > 0; i--)
    {
      delay(1000);
      Serial.print(i);
      Serial.print(" ");
    }
    Serial.print("\n");
}
    
void runCommand(String s, bool debug){
  if (s == "Enter Command Mode")
  {
      SpiSerial.print("$$$");  
      commandMode = true;
      delay(250);
      SpiSerial.print("\r");
  }
  else if (s == "Quit Command Mode")
  {
      SpiSerial.print("exit\r");
      commandMode = false;  
      delay(1000);  
  }
  else
  {
      SpiSerial.print(s);    // print s to SPI serial
      SpiSerial.print("\r");
  }
  
  if (debug)
  {   Serial.print("\t");
      Serial.print(s);
      Serial.print(" : "); 
  }
  
  if (readFromDevice() == true && debug == true)
  {
	Serial.print(" OK \n");  
  }
  else if (debug == true)
  {
	Serial.print(" ERROR \n");
  }
}

bool readFromDevice(void)
{
	bool aok = false;
	char spiReturn[1024];
	char * pointer = &spiReturn[0];
	int i = 0;
 
	delay(500);
 
	while(SpiSerial.available() > 0) {
		*pointer = SpiSerial.read();
		if (*(pointer-1) == 'O' && *pointer == 'K')
			aok = true;
		if (*(pointer-3) == 'E' && *(pointer-2) == 'X' && *(pointer-1) == 'I' && *(pointer) == 'T')
			aok = true;
		pointer++;
	}
	return aok; 
}


void createCommand(unsigned long *command, unsigned long *data, int commandNum, int playerID, int deviceType, int expansion, int dataVal)
{
  *command |= (((unsigned long)expansion & 0x1F) << 27);
  *command |= (((unsigned long)deviceType & 0x1F) << 22);
  *command |= (((unsigned long)playerID & 0x3FF) << 12);
  *command |= ((unsigned long)0x2 << 8);
  *command |= ((unsigned long)commandNum & 0xFF); 
  *data = dataVal;   
}

void splitCommand(byte bArray[], unsigned long command, unsigned long data)
{
  // Create Command Byte Array
  bArray[0] = (command & 0xFF000000) >> 24;
  bArray[1] = (command & 0x00FF0000) >> 16;
  bArray[2] = (command & 0x0000FF00) >> 8;
  bArray[3] = (command & 0x000000FF) >> 00;

  // Create Data Byte Array
  bArray[4] = (data & 0xFF000000) >> 24;
  bArray[5] = (data & 0x00FF0000) >> 16;
  bArray[6] = (data & 0x0000FF00) >> 8;
  bArray[7] = (data & 0x000000FF) >> 00;   
}



